package com.sdg.scala

/**
  * Created by vara on 13/10/2016.
  */

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ScalaSuite extends FunSuite{

  import Main.add
  import Main.pascal
  test("pascal: col=0,row=2") {
    assert(pascal(0,2) === 1)
  }

  test("pascal: col=1,row=2") {
    assert(pascal(1,2) === 2)
  }

  test("pascal: col=1,row=3") {
    assert(pascal(1,3) === 3)
  }

  test("pascal: col=3, row=5"){

    assert(pascal(3,5)===15)
  }

 test("add:a=5, b=4")
   {
   assert(add(5,6) == 12)
 }

}
